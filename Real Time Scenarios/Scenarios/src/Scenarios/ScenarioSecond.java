package Scenarios;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ScenarioSecond {

	public static void main(String[] args) throws InterruptedException {

		// declaration and instantiation of objects/variables
		System.setProperty("webdriver.chrome.driver",
				"D:\\BBD Testing Program\\chromedriver_win32 (1)\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		// Maximize the browser window.
		driver.manage().window().maximize();

		// Navigate to �https://in.yahoo.com/�.
		driver.get("https://in.yahoo.com/");

		// Verify text "Trending Now"
		List<WebElement> l = driver.findElements(By.xpath("//*[@id=\"bd\"]/ol[2]/li/div/div/ul/li/div[1]/h4"));

		String url = "";
		List<WebElement> allURLs = driver.findElements(By.className("icon-find"));

		System.out.println("Total links on the Wb Page: " + allURLs.size());

		for (int i = 0; i < allURLs.size(); i++)

		{

			System.out.println(allURLs.get(i).getText());

		}

		for (int j = 0; j < allURLs.size(); j++) {

			WebElement footer1New = driver.findElement(By.xpath("//*[@id=\"bd\"]/ol[2]/li/div/div/ul/li/div[2]"));
			List<WebElement> allURLsNew = footer1New.findElements(By.tagName("a"));
			allURLsNew.get(j).click();

			driver.navigate().back();
			Thread.sleep(3000);

		}

		for (int j = 0; j < allURLs.size(); j++) {
			WebElement New = driver.findElement(By.xpath("//*[@id=\"bd\"]/ol[1]/li/div/div/ul/li[1]/div/div"));
			List<WebElement> allURLsNew1 = New.findElements(By.tagName("a"));
			allURLsNew1.get(j).click();

			String urlToNavigate = " https://covid19.who.int/";
			driver.navigate().to(urlToNavigate);

			Thread.sleep(2000);

			driver.findElement(By.xpath(
					"//div[@class='dropdown__control dropdown__control--is-focused css-1hfqf2a-control']//div[@class='dropdown__value-container css-1hwfws3']"))
					.sendKeys("ind");
			Thread.sleep(2000);
			List<WebElement> options = driver.findElements(By.cssSelector("#react-select-7-input"));

			for (WebElement option : options)

			{

				if (option.getText().equalsIgnoreCase("India"))

				{

					option.click();

					break;
				}

			}
		}
		driver.quit();
	}

}
