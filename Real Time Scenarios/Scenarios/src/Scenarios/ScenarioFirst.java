package Scenarios;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ScenarioFirst {

	public static void main(String[] args) {									
		
    	// declaration and instantiation of objects/variables		
        System.setProperty("webdriver.chrome.driver","D:\\BBD Testing Program\\chromedriver_win32 (1)\\chromedriver.exe");					
        WebDriver driver = new ChromeDriver();					

        //Maximize the browser window. 
        driver.manage().window().maximize(); 
        
        //Navigate to �http://qatechhub.com�.
        driver.get("http://qatechhub.com/");
        
        //Print the title of the Page.
        System.out.println("Page title is : " + driver.getTitle());
        
        //Print the current URL.
        String strUrl1 = driver.getCurrentUrl();
        System.out.println("Current Url is:"+ strUrl1);
        
        //Navigate to the Facebook page (https://www.facebook.com)
        driver.get("https://www.facebook.com/");
        
        //Navigate back to the QA Tech Hub website.
    	driver.navigate().back();
    	
    	//Print the URL of the current page.
    	String strUrl2 = driver.getCurrentUrl();
        System.out.println("Current Url is:"+ strUrl2);

        //Navigate forward.
        driver.navigate().forward();
        
        //Reload the page.
        driver.navigate().refresh();
        
        //Close the Browser.
        driver.close();
        
        
	}
}
