package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

@Test
public class Wishlist {
	
	public WebDriver driver;
	
	By Wishlist=By.xpath("//*[@id=\"appContent\"]/div/div/div/div/div[1]/div[3]/div[3]/div[2]/button");	

    public Wishlist(WebDriver driver) {
		// TODO Auto-generated constructor stub
    	
    	this.driver=driver;
	}



	public WebElement getWishlist() {
		// TODO Auto-generated method stub
		return driver.findElement(Wishlist);
		//return null;
	}
}
