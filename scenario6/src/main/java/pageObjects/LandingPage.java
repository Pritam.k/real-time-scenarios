package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

@Test
public class LandingPage {
	
	public WebDriver driver;
	
	By men=By.cssSelector("a[href*='men']");
	
	

    public LandingPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
    	
    	this.driver=driver;
	}




	public WebElement getPage() {
    	return driver.findElement(men);
    }




	public boolean getNavigationBar() {
    	return driver.findElement(men) != null;

		// TODO Auto-generated method stub
		//return null;
	}
}
