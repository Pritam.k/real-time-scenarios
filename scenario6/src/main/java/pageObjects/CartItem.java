package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CartItem {
	
	public WebDriver driver;
	
	By Size=By.xpath("//*[@id=\"sizeButtonsContainer\"]/div[2]/div/div/button");

	By Cart=By.xpath("//*[@id=\"mountRoot\"]/div/div/div/main/div[2]/div[2]/div[3]/div/div[1]");

    public CartItem(WebDriver driver) {
		// TODO Auto-generated constructor stub
    	
    	this.driver=driver;
	}




	public WebElement getSize() {
    	return driver.findElement(Size);
    }
    




	public WebElement getCartItem() {
		// TODO Auto-generated method stub
		return driver.findElement(Cart);
		//return null;
	}
}
