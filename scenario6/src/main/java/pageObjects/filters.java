package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
@Test
public class filters {
	
	public WebDriver driver;
	
	By Gender=By.xpath("//*[@id=\"mountRoot\"]/div/div[1]/main/div[3]/div[1]/section/div/div[3]/ul/li[2]/label");	
    By Categories=By.xpath("/html/body/div[2]/div/div[1]/main/div[3]/div[1]/section/div/div[3]/ul/li[2]/label");
    By Brand=By.xpath("//*[@id=\"mountRoot\"]/div/div[1]/main/div[3]/div[1]/section/div/div[4]/ul/li[6]/label");
    By Price=By.xpath("//*[@id=\"mountRoot\"]/div/div[1]/main/div[3]/div[1]/section/div/div[5]/ul/li[1]/label");
    By Color=By.xpath("//*[@id=\"mountRoot\"]/div/div[1]/main/div[3]/div[1]/section/div/div[6]/ul/li[2]/label");
    public filters(WebDriver driver) {
		// TODO Auto-generated constructor stub
    	
    	this.driver=driver;
	}




	//public WebElement getProductPage() {
    //	return driver.findElement(Product);
   // }




	public WebElement getGender() {
		// TODO Auto-generated method stub
		return driver.findElement(Gender);
		
	
	}

	public WebElement getCategories() {
		return driver.findElement(Categories);
	
	}

	public WebElement getBrand() {
		return driver.findElement(Brand);
	
	}

	public WebElement getPrice() {
		return driver.findElement(Price);
	
	}

	public WebElement getColor() {
		return driver.findElement(Color);
	
	}
	
}
