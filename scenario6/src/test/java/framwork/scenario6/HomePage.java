package framwork.scenario6;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import java.io.IOException;

import org.testng.annotations.Test;

import pageObjects.CartItem;
import pageObjects.LandingPage;
import pageObjects.PurchaseItem;
import pageObjects.SelectItem;
import pageObjects.SortBy;
import pageObjects.Wishlist;
import pageObjects.filters;
import resources.base;
@Test
public class HomePage extends base {

	 public static Logger log =LogManager.getLogger(base.class.getName());
	public void initialize() throws IOException
	{
	
		 driver =initializeDriver();
		 driver.get(prop.getProperty("url"));
		  //driver.get("https://www.myntra.com/");

		 

	}
	
	@Test 
	public void basePageNavigation() throws IOException
	
	{
	    driver = initializeDriver();
	    driver.get("https://www.myntra.com/");
	    
	    LandingPage l = new LandingPage(driver);
	    l.getPage().click();
	    
	    PurchaseItem P=new PurchaseItem(driver);
	    P.getProductPage().click();
	    
	    SortBy S=new SortBy(driver);
	    S.getSortBy().click();
	    
	    filters F=new filters(driver);
	    F.getGender().click();
	    F.getCategories().click();
	    F.getBrand().click();
	    F.getPrice().click();
	    F.getColor().click();
	    
	    SelectItem Si= new SelectItem(driver);
	    Si.getSelect().click();
	    
	    CartItem Si1=new CartItem(driver);
		Si1.getSize().click();

	    
	    CartItem C = new CartItem(driver);
	    C.getCartItem().click();
	    
	    Wishlist W = new Wishlist(driver);
	    W.getWishlist().click();
	    
	    
	}
	@AfterTest
	public void teardown()
	{
		
		driver.close();
	
		
	}


}
